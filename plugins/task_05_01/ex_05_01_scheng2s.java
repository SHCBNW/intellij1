import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

import java.util.ArrayList;

@SuppressWarnings({"WrongPackageStatement", "Duplicates", "Convert2Diamond", "SuspiciousNameCombination", "SameParameterValue"})
public class ex_05_01_scheng2s implements PlugInFilter {

    private String titlePrefix = "Sobel Filter by Siu Cheng";
    private ImageProcessor originalImage, resultImage;
    private int width, height;

    // setup variables
    private ArrayList<Operator> operators = new ArrayList<Operator>();
    private boolean invertFlag, rotate45Flag = false, rotate90Flag = false, rotate180Flag = false;
    private int repeat = 0;

    public int setup(String arg, ImagePlus im) {
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        try {
            showDialog1();
        } catch (RuntimeException e) { // in case the user click cancel
            return;
        }
        long millis = System.currentTimeMillis();
        this.width = ip.getWidth();
        this.height = ip.getHeight();
        this.originalImage = isGrey(ip) ? ip : colorToGrey(ip);
        this.resultImage = this.originalImage.createProcessor(this.width, this.height);
        calc();
        if (this.invertFlag) this.resultImage = invertGrey(this.resultImage);
        millis = System.currentTimeMillis() - millis;
        new ImagePlus(this.titlePrefix + " - time = " + millis + " ms", this.resultImage).show();
    }

    private void showDialog1() {
        IJ.showStatus("Status: Waiting");
        GenericDialog gd = new GenericDialog(this.titlePrefix);
        String[] items = {"Yes    ", "No"};
        gd.addRadioButtonGroup("Use Default Sobel Operator", items, 1, items.length, items[0]);
        gd.addRadioButtonGroup("Invert Result Image", items, 1, items.length, items[1]);
        gd.showDialog();
        if (gd.wasCanceled()) throw new RuntimeException("Cancel");
        boolean defaultStructure = gd.getNextRadioButton().startsWith("Y");
        this.invertFlag = gd.getNextRadioButton().startsWith("Y");
        if (defaultStructure) {
            this.operators.add(new Operator()); // sobeldefault hx
            this.operators.add(new Operator().rotate180().rotate90()); // sobeldefault hy
        } else showDialog2();
    }

    private void showDialog2() {
        GenericDialog gd = new GenericDialog(this.titlePrefix + " - Set Elements");
        for (int x = 0; x < 3; x++)
            for (int y = 0; y < 3; y++) {
                gd.addNumericField("", 0, 0);
                if (y < 2) gd.addToSameRow();
            }
        String[] items = {"Yes    ", "No"};
        gd.addRadioButtonGroup("Add Another Operator?", items, 1, items.length, items[1]);
        gd.showDialog();
        if (gd.wasCanceled()) throw new RuntimeException("Cancel");
        byte[][] operator = new byte[3][3];
        for (int x = 0; x < 3; x++)
            for (int y = 0; y < 3; y++)
                operator[x][y] = (byte) gd.getNextNumber(); // get all inputs and put it in a array
        this.operators.add(new Operator(operator));
        if (gd.getNextRadioButton().startsWith("Y")) showDialog2(); // one more operator
        else if (this.operators.size() == 1) showDialog3();
    }

    private void showDialog3() {
        GenericDialog gd = new GenericDialog(this.titlePrefix + " - Set Rotation");
        String[] items = {"No Rotation", "Yes, 45° Rotation", "Yes, 90° Rotation", "Yes, 180° Rotation"};
        gd.addRadioButtonGroup("Do you want to rotate your operator and use it again?",
                items, items.length, 1, items[0]);
        gd.addNumericField("How many times do you want to repeat your rotation?", 0, 0);
        gd.showDialog();
        if (gd.wasCanceled()) throw new RuntimeException("Cancel");
        String rotation = gd.getNextRadioButton();
        if (rotation.contains("N")) return;
        this.rotate45Flag = rotation.contains("4");
        this.rotate90Flag = rotation.contains("9");
        this.rotate180Flag = rotation.contains("8");
        this.repeat += (int) gd.getNextNumber();
    }

    private void calc() {
        IJ.showStatus("Status: Calculating");
        for (int x = 0; x < this.width; x++) {
            IJ.showProgress(x, this.width - 1);
            for (int y = 0; y < this.height; y++) {
                ArrayList<Integer> resultsFromSubCalc = new ArrayList<Integer>();
                for (int repeat = -1; repeat < this.repeat; repeat++) // repeat commands x times or at least one time
                    for (int element = 0; element < this.operators.size(); element++) {
                        resultsFromSubCalc.add(subCalc(x, y, this.operators.get(element)));
                        if (this.operators.size() == 1) // rotation is only available for single operator
                            if (this.rotate45Flag) this.operators.get(0).rotate45();
                            else if (this.rotate90Flag) this.operators.get(0).rotate90();
                            else if (this.rotate180Flag) this.operators.get(0).rotate180();
                    }
                int sum = 0;
                for (Integer tmp : resultsFromSubCalc) sum += tmp * tmp;
                int grey = checkRange((int) Math.sqrt(sum), 0, 255);
                this.resultImage.putPixel(x, y, new int[]{grey, grey, grey});
                if (this.operators.size() == 1 && (this.rotate45Flag || this.rotate90Flag || this.rotate180Flag))
                    this.operators.get(0).setOriginal(); // rotate back to original
            }
        }
    }

    private int subCalc(int picX, int picY, Operator operator) {
        int sum = 0;
        for (byte x = 0; x < 3; x++)
            for (byte y = 0; y < 3; y++)
                sum += (operator.get(y, x) // operator * grey value
                        * getGreyAtPos(this.originalImage, picX + x - 1, picY + y - 1));
        return sum;
    }

    private int getGreyAtPos(ImageProcessor p, int x, int y) {
        return p.getPixel(
                checkRange(x, 0, this.width - 1), // avoids ArrayOutOfBounce
                checkRange(y, 0, this.height - 1)) // avoids ArrayOutOfBounce
                & 0x0000ff;
    }

    private int checkRange(int check, int min, int max) {
        return check < min ? min : check > max ? max : check;
    }

    // some helper
    private boolean isGrey(ImageProcessor ip) {
        int[] rgb = new int[3];
        for (int x = 0; x < this.width; x++)
            for (int y = 0; y < this.height; y++) {
                ip.getPixel(x, y, rgb);
                if (!(rgb[0] == rgb[1] && rgb[1] == rgb[2])) return false;
            }
        return true;
    }

    private ImageProcessor invertGrey(ImageProcessor ip) {
        ImageProcessor invertedIP = ip.createProcessor(this.width, this.height);
        for (int x = 0; x < this.width; x++) {
            IJ.showProgress(x, this.width);
            for (int y = 0; y < this.height; y++) {
                int grey = 255 - (getGreyAtPos(ip, x, y));
                invertedIP.putPixel(x, y, new int[]{grey, grey, grey});
            }
        }
        return invertedIP;
    }

    private ImageProcessor colorToGrey(ImageProcessor ip) {
        ImageProcessor greyIP = ip.createProcessor(this.width, this.height);
        int[] rgb = new int[3];
        for (int x = 0; x < this.width; x++)
            for (int y = 0; y < this.height; y++) {
                ip.getPixel(x, y, rgb);
                int grey = (int) (0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2]);
                greyIP.putPixel(x, y, new int[]{grey, grey, grey});
            }
        return greyIP;
    }

    @SuppressWarnings({"unused", "UnusedReturnValue"})
    class Operator {
        private byte[][] elements;
        private byte[][] original;

        Operator() { // if no operator is submitted the constructor will take calc as default
            setDefaultSobel();
            this.original = this.elements;
        }

        Operator(byte[][] elements) {
            this.elements = elements;
            this.original = this.elements;
        }

        private byte[][] get() {
            return elements;
        }

        private byte get(byte x, byte y) {
            return this.elements[x][y];
        }

        private Operator setDefaultSobel() {
            this.elements = new byte[][]{ // hx
                    {1, 2, 1},
                    {0, 0, 0},
                    {-1, -2, -1}};
            return this;
        }

        private Operator setOriginal() {
            this.elements = this.original;
            return this;
        }

        private Operator rotate45() {
            byte[][] newArray = new byte[3][3];
            newArray[1][1] = this.elements[1][1];
            newArray[1][0] = this.elements[2][0];
            newArray[2][0] = this.elements[2][1];
            newArray[2][1] = this.elements[2][2];
            newArray[2][2] = this.elements[1][2];
            newArray[1][2] = this.elements[0][2];
            newArray[0][2] = this.elements[0][1];
            newArray[0][1] = this.elements[0][0];
            newArray[0][0] = this.elements[1][0];
            this.elements = newArray;
            return this;
        }

        private Operator rotate90() {
            rotate45().rotate45();
            return this;
        }

        private Operator rotate180() {
            rotate90().rotate90();
            return this;
        }

        private Operator flipHorizontal() {
            byte tmp;
            for (int i = 0; i < 3; i++) {
                tmp = elements[0][i];
                elements[0][i] = elements[2][i];
                elements[2][i] = tmp;
            }
            return this;
        }

        private Operator flipVertical() {
            rotate90().flipHorizontal().rotate180().rotate90();
            return this;
        }

        private Operator print() {
            StringBuilder s = new StringBuilder();
            for (int x = 0; x < 3; x++) {
                for (int y = 0; y < 3; y++)
                    s.append(elements[x][y]).append(" ");
                s.append("\n");
            }
            IJ.log(s.toString());
            return this;
        }
    }

//    public static void main(String[] args) {
//        new Operator(new byte[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}).print()
//                .rotate45().print()
//                .rotate90().print()
//                .rotate180().print()
//                .flipHorizontal().print()
//                .flipVertical().print()
//                .setOriginal().print()
//                .setDefaultSobel().print();
//    }

//    public static void main(String[] args) {
//        new Operator(new byte[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}).print()
//                .rotate90().print()
//                .rotate90().print()
//                .rotate90().print();
//    }
}