// package plugins.task_05_01;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

import java.util.ArrayList;

@SuppressWarnings({"WrongPackageStatement", "Duplicates", "Convert2Diamond", "SuspiciousNameCombination", "SameParameterValue", "unused"})
public class ex_05_01_scheng2s_Para implements PlugInFilter {

    private ImagePlus imagePlus;
    private String titlePrefix = "Sobel Filter by Siu Cheng";
    private volatile ImageProcessor resultImage;
    private int width, height;

    // setup variables
    private ArrayList<Operator> operators = new ArrayList<Operator>();
    private int repeat = 0, cores;
    private boolean invertFlag, rotate45Flag = false, rotate90Flag = false, rotate180Flag = false;


    public int setup(String arg, ImagePlus im) {
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        try {
            showDialog1();
        } catch (RuntimeException e) { // in case the user clicks cancel
            return;
        }
        long millis = System.currentTimeMillis();
        width = ip.getWidth();
        height = ip.getHeight();
        ip = isGrey(ip) ? ip : colorToGrey(ip);


        resultImage = ip.createProcessor(width, height);
        IJ.showStatus("Status: Calculating");

        imagePlus = new ImagePlus("", resultImage);
        imagePlus.show();
        
        // fork

        // ExecutorService executorService = Executors.newFixedThreadPool(cores);
        // CompletionService<Calculator> taskCompletionService = new ExecutorCompletionService<Calculator>(executorService);

        int jump = width / cores;

        ArrayList<Thread> threads = new ArrayList<Thread>();

        for (int i = 0; i < cores; i++) {
            ArrayList<Operator> copyList = new ArrayList<Operator>();
            for (Operator operator : operators) {
                try {
                    copyList.add(operator.clone());
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
            Runnable task = new Calculator(
                    i * jump,
                    (i == cores - 1) ? (i * jump) + jump - 1 : width - 1,
                    0,
                    height - 1,
                    copyList,
                    ip.duplicate()
            );

            Thread worker = new Thread(task);
            worker.start();
            threads.add(worker);
        }

        // join
        for (Thread thread : threads) {
            try {
                thread.join(); // GET THREADS BACK
            } catch (InterruptedException e) {
                // e.printStackTrace();
            }
        }

        // System.out.println("done");
        if (invertFlag) resultImage = invertGrey(resultImage);
        millis = System.currentTimeMillis() - millis;

        // imagePlus = new ImagePlus("", resultImage);
        imagePlus.setTitle(titlePrefix + " - time = " + millis + " ms, Cores: " + cores);
        //  imagePlus.show();
        imagePlus.draw();
    }

    private void showDialog1() {
        IJ.showStatus("Status: Waiting");
        // int availableProcessors = Runtime.getRuntime().availableProcessors();
        GenericDialog gd = new GenericDialog(titlePrefix);
        String[] items = {"Yes    ", "No"};
        gd.addRadioButtonGroup("Use Default Sobel Operator", items, 1, items.length, items[0]);
        gd.addRadioButtonGroup("Invert Result Image", items, 1, items.length, items[1]);
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        gd.addSlider("Threads for calculation:", 1, availableProcessors, availableProcessors);
        gd.showDialog();
        if (gd.wasCanceled()) throw new RuntimeException("Cancel");
        boolean defaultStructure = gd.getNextRadioButton().startsWith("Y");
        invertFlag = gd.getNextRadioButton().startsWith("Y");
        cores = (int) gd.getNextNumber();
        if (defaultStructure) {
            operators.add(new Operator()); // sobeldefault hx
            operators.add(new Operator().rotate180().rotate90()); // sobeldefault hy
        } else showDialog2();
    }

    private void showDialog2() {
        GenericDialog gd = new GenericDialog(titlePrefix + " - Set Elements");
        for (int x = 0; x < 3; x++)
            for (int y = 0; y < 3; y++) {
                gd.addNumericField("", 0, 0);
                if (y < 2) gd.addToSameRow();
            }
        String[] items = {"Yes    ", "No"};
        gd.addRadioButtonGroup("Add Another Operator?", items, 1, items.length, items[1]);
        gd.showDialog();
        if (gd.wasCanceled()) throw new RuntimeException("Cancel");
        byte[][] operator = new byte[3][3];
        for (int x = 0; x < 3; x++)
            for (int y = 0; y < 3; y++)
                operator[x][y] = (byte) gd.getNextNumber(); // get all inputs and put it in a array
        operators.add(new Operator(operator));

        System.out.println(operators.size());
        if (gd.getNextRadioButton().startsWith("Y")) showDialog2(); // one more operator
        else if (operators.size() == 1) showDialog3();
    }

    private void showDialog3() {
        GenericDialog gd = new GenericDialog(titlePrefix + " - Set Rotation");
        String[] items = {"No Rotation", "Yes, 45° Rotation", "Yes, 90° Rotation", "Yes, 180° Rotation"};
        gd.addRadioButtonGroup("Do you want to rotate your operator and use it again?",
                items, items.length, 1, items[0]);
        gd.addNumericField("How many times do you want to repeat your rotation?", 0, 0);
        gd.showDialog();
        if (gd.wasCanceled()) throw new RuntimeException("Cancel");
        String rotation = gd.getNextRadioButton();
        if (rotation.contains("N")) return;
        rotate45Flag = rotation.contains("4");
        rotate90Flag = rotation.contains("9");
        rotate180Flag = rotation.contains("8");
        repeat += (int) gd.getNextNumber();
    }

    // some helper

    private int checkRange(int check, int min, int max) {
        return check < min ? min : check > max ? max : check;
    }

    private int getGreyAtPos(ImageProcessor p, int x, int y) {
        return p.getPixel(
                checkRange(x, 0, width - 1), // avoids ArrayOutOfBounce
                checkRange(y, 0, height - 1)) // avoids ArrayOutOfBounce
                & 0x0000ff;
    }

    private boolean isGrey(ImageProcessor ip) {
        int[] rgb = new int[3];
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++) {
                ip.getPixel(x, y, rgb);
                if (!(rgb[0] == rgb[1] && rgb[1] == rgb[2])) return false;
            }
        return true;
    }

    private ImageProcessor invertGrey(ImageProcessor ip) {
        ImageProcessor invertedIP = ip.createProcessor(width, height);
        for (int x = 0; x < width; x++) {
            IJ.showProgress(x, width);
            for (int y = 0; y < height; y++) {
                int grey = 255 - (getGreyAtPos(ip, x, y));
                invertedIP.putPixel(x, y, new int[]{grey, grey, grey});
            }
        }
        return invertedIP;
    }

    private ImageProcessor colorToGrey(ImageProcessor ip) {
        ImageProcessor greyIP = ip.createProcessor(width, height);
        int[] rgb = new int[3];
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++) {
                ip.getPixel(x, y, rgb);
                int grey = (int) (0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2]);
                greyIP.putPixel(x, y, new int[]{grey, grey, grey});
            }
        return greyIP;
    }

    @SuppressWarnings({"unused", "UnusedReturnValue"})
    class Operator {
        private byte[][] elements;
        private byte[][] original;

        Operator() { // if no operator is submitted the constructor will take calc as default
            setDefaultSobel();
            this.original = this.elements;
        }

        Operator(byte[][] elements) {
            this.elements = elements;
            this.original = this.elements;
        }

        private byte[][] get() {
            return elements;
        }

        private byte get(byte x, byte y) {
            return this.elements[x][y];
        }

        private Operator setDefaultSobel() {
            this.elements = new byte[][]{ // hx
                    {1, 2, 1},
                    {0, 0, 0},
                    {-1, -2, -1}};
            return this;
        }

        private Operator setOriginal() {
            this.elements = this.original;
            return this;
        }

        private Operator rotate45() {
            byte[][] newArray = new byte[3][3];
            newArray[1][1] = this.elements[1][1];
            newArray[1][0] = this.elements[2][0];
            newArray[2][0] = this.elements[2][1];
            newArray[2][1] = this.elements[2][2];
            newArray[2][2] = this.elements[1][2];
            newArray[1][2] = this.elements[0][2];
            newArray[0][2] = this.elements[0][1];
            newArray[0][1] = this.elements[0][0];
            newArray[0][0] = this.elements[1][0];
            this.elements = newArray;
            return this;
        }

        private Operator rotate90() {
            return rotate45().rotate45();
        }

        private Operator rotate180() {
            return rotate90().rotate90();
        }

        private Operator flipHorizontal() {
            byte tmp;
            for (int i = 0; i < 3; i++) {
                tmp = elements[0][i];
                elements[0][i] = elements[2][i];
                elements[2][i] = tmp;
            }
            return this;
        }

        private Operator flipVertical() {
            return rotate90().flipHorizontal().rotate180().rotate90();
        }

        private Operator print() {
            StringBuilder s = new StringBuilder();
            for (int x = 0; x < 3; x++) {
                for (int y = 0; y < 3; y++)
                    s.append(elements[x][y]).append(" ");
                s.append("\n");
            }
            IJ.log(s.toString());
            return this;
        }

        @SuppressWarnings({"MethodDoesntCallSuperMethod", "RedundantThrows"})
        protected Operator clone() throws CloneNotSupportedException {
            Operator o = new Operator();
            o.elements = this.elements.clone();
            o.original = this.original.clone();
            return o;
        }
    }

    class Calculator implements Runnable {
        final int start_X, end_X, start_Y, end_Y;
        final ArrayList<Operator> operators;
        final ImageProcessor originalImage;

        Calculator(int start_x, int end_x, int start_y, int end_y, ArrayList<Operator> operators, ImageProcessor originalImage) {
            start_X = start_x;
            end_X = end_x;
            start_Y = start_y;
            end_Y = end_y;
            this.operators = operators;
            this.originalImage = originalImage;
        }

        @Override
        public void run() {
            for (int x = this.start_X; x <= this.end_X; x++) {
                // IJ.showProgress(x, width - 1);
                for (int y = this.start_Y; y < this.end_Y; y++) {
                    ArrayList<Integer> resultsFromSubCalc = new ArrayList<Integer>();
                    for (int repeater = -1; repeater < repeat; repeater++) // repeat commands x times or at least one time
                        for (int element = 0; element < this.operators.size(); element++) {
                            resultsFromSubCalc.add(calc(x, y, operators.get(element)));
                            if (operators.size() == 1) // rotation is only available for single operator
                                if (rotate45Flag) this.operators.get(0).rotate45();
                                else if (rotate90Flag) this.operators.get(0).rotate90();
                                else if (rotate180Flag) this.operators.get(0).rotate180();
                        }
                    int sum = resultsFromSubCalc.stream().mapToInt(tmp -> tmp * tmp).sum();
                    int grey = checkRange((int) Math.sqrt(sum), 0, 255);
                    resultImage.putPixel(x, y, new int[]{grey, grey, grey});
                    if (operators.size() == 1 && (rotate45Flag || rotate90Flag || rotate180Flag))
                        operators.get(0).setOriginal(); // rotate back to original
                }
                imagePlus.draw();
            }
        }

        private int calc(int picX, int picY, Operator operator) {
            int sum = 0;
            for (byte x = 0; x < 3; x++)
                for (byte y = 0; y < 3; y++)
                    sum += (operator.get(y, x) // operator * grey value
                            * getGreyAtPos(this.originalImage, picX + x - 1, picY + y - 1));
            return sum;
        }

    }

//    public static void main(String[] args) {
//        new Operator(new byte[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}).print()
//                .rotate45().print()
//                .rotate90().print()
//                .rotate180().print()
//                .flipHorizontal().print()
//                .flipVertical().print()
//                .setOriginal().print()
//                .setDefaultSobel().print();
//    }

//    public static void main(String[] args) {
//        new Operator(new byte[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}).print()
//                .rotate90().print()
//                .rotate90().print()
//                .rotate90().print();
//    }

}