
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Aufgabe:
 * 1.Erstellen Sie ein ImageJ-Plugin, das ein RGB-Farbbild auf verschiedene Art in ein 8bit-Graustufenbild umwandelt:
 * a) mit gleichen Beiträgen aller Farbkanäle und
 * b) mit physiologisch bereinigten Helligkeitsbeiträgen der Farbkanäle (vgl. Luminanz in YUV).
 * Wenden Sie dieses Plugin auf ein Beispielbild an und vergleichen Sie beide Graubilddarstellungen. (1 BP)
 */

@SuppressWarnings("WrongPackageStatement")
public class ex_01_01_scheng2s implements PlugInFilter {
    ImagePlus imp;
    ImageProcessor ip;

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.ip = ip;
        colorToGreyscale();
    }

    private void colorToGreyscale() {
        GenericDialog gd = new GenericDialog("Ü01A01 by Siu Ho Cheng");
        gd.addMessage("Please choose the task:");
        String[] s = {"A", "B"};
        gd.addChoice("Task: ", s, s[0]);
        gd.showDialog();
        if (gd.wasCanceled()) return;

        // Set 0 for task a) or 1 for task b)
        int setup = gd.getNextChoiceIndex();

        int w = ip.getWidth();
        int h = ip.getHeight();
        ImageProcessor newPicture = this.ip.createProcessor(w, h);

        int[] rgb = new int[3];
        for (int u = 0; u < w; u++) {
            for (int v = 0; v < h; v++) {
                ip.getPixel(u, v, rgb);
                // setup affects the following line
                int tmp = setup == 0 ? (rgb[0] + rgb[1] + rgb[2]) / 3 : (int) (0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2]);
                //IJ.log(Arrays.toString(rgb));
                newPicture.putPixel(u, v, new int[]{tmp, tmp, tmp});
            }
        }
        new ImagePlus("BW-picture", newPicture).show();

    }
}