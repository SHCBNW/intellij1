
import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Task:
 * Write a cross correlation filter of size 3x3 pixels, calculating within the kernel mask the maximum
 * difference of any colour channel value to the luminance (weighted brightness) for the central kernel pixel and
 * display the result als grey value on this pixel. Apply this filter to a sample colour image. (2 BP)
 */

@SuppressWarnings({"SpellCheckingInspection", "ConstantConditions", "SameParameterValue", "SillyAssignment", "unused", "CStyleArrayDeclaration", "WrongPackageStatement"})
public class ex_03_01_scheng2s implements PlugInFilter {
    ImagePlus imp;
    ImageProcessor ip;
    boolean showTimeInLogs, showFilterSizeInLogs;

    public ex_03_01_scheng2s() {
    }

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.ip = ip;
        showOptionsMenu();
    }

    private void showOptionsMenu() {
        GenericDialog gd = new GenericDialog("Convolution Filter by Siu Ho Cheng");
        gd.addMessage("Please choose the size of the filter:");
        gd.addSlider("X", 3, 20, 3);
        gd.addSlider("Y", 3, 20, 3);
        gd.addCheckbox("Show Time", true);
        gd.addCheckbox("Show Filtersize", false);
        gd.showDialog();
        if (gd.wasCanceled())
            return;
        this.showTimeInLogs = gd.getNextBoolean();
        this.showFilterSizeInLogs = gd.getNextBoolean();
        coordinateHelper((int) gd.getNextNumber(), (int) gd.getNextNumber());
    }

    private void coordinateHelper(int filterSizeX, int filterSizeY) {
        long millis = 0;
        int upLeftCornerX, upLeftCornerY, lowRightCornerX, lowRightCornerY;
        boolean isEvanX = filterSizeX % 2 == 0, isEvanY = filterSizeY % 2 == 0;
        if (showTimeInLogs) millis = System.currentTimeMillis();
        int w = ip.getWidth(), h = ip.getHeight();
        ImageProcessor newPicture = ip.createProcessor(w, h);
        for (int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {
                // Define area with X an Y coordinates
                // Method checkRange() prevents ArrayOutOfBounce
                upLeftCornerX = checkRange(x - filterSizeX / 2, w - 1);
                upLeftCornerY = checkRange(y - filterSizeY / 2, h - 1);
                lowRightCornerX = checkRange(x + (isEvanX ? (filterSizeX - 1) : filterSizeX) / 2, w - 1);
                lowRightCornerY = checkRange(y + (isEvanY ? (filterSizeY - 1) : filterSizeY) / 2, h - 1);
                // Calculate Luv differences
                int luvDiff = getLuvDiff(upLeftCornerX, upLeftCornerY, lowRightCornerX, lowRightCornerY, x, y);
                // Return calculated value in newPicture
                newPicture.putPixel(x, y, new int[]{luvDiff, luvDiff, luvDiff});
                if (showFilterSizeInLogs)
                    IJ.log("Check: (" + x + "," + y
                            + "), Start: (" + upLeftCornerX + "," + upLeftCornerY
                            + "), End: (" + lowRightCornerX + "," + lowRightCornerY + ")");
            }
        }
        millis = System.currentTimeMillis() - millis;
        new ImagePlus(filterSizeX + "x" + filterSizeY + " - " + millis + " ms", newPicture).show();
        if (showTimeInLogs) IJ.log(millis + " ms needed to calculate new picture.\n"
                + w * h + " pixels checked.");
    }

    private int getLuvDiff(int upLeftCornerX, int upLeftCornerY, int lowRightCornerX, int lowRightCornerY, int centerX, int centerY) {
        int high = Integer.MIN_VALUE, rgb[] = new int[3];
        ip.getPixel(centerX, centerY, rgb);
        int centerPix = (int) (0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2]);

        for (int x = upLeftCornerX; x <= lowRightCornerX; x++)
            for (int y = upLeftCornerY; y <= lowRightCornerY; y++) {
                ip.getPixel(x, y, rgb);
                int luv = (int) (0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2]);
                // Find highest luv
                high = luv > high ? luv : high;
                if (high == 255)
                    break; // Saves time
            }
        return high - centerPix;
    }

    // Separated checkRange() might be helpful later on
    private int checkRange(int value, int max) {
        return checkRange(value, 0, max);
    }

    private int checkRange(int value, int min, int max) {
        return (value < min) ? min : (value > max) ? max : value;
    }
}