package plugins.hypermedia_task_05;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//     Schreiben sie ein Program, z.B. ein ImageJ plugin, das:
//         - Daten mit dem einem der Algorithmen LZW oder LZSS komprimiert (1)
//         -  ... und auch wieder dekomprimiert (1).
//         - Das Programm soll ein Bild oder einen Text einlesen, komprimieren und beim Dekomprimieren so einfärben,
//           dass Zeichen/Pixel, die aus dem gleichen Symbol dekomprimiert wurden, die gleiche Farbigkeit haben.
//           Benachbarte Symbole sollen gut unterscheidbar eingefärbt werden. Zeigen Sie ein Beispiel. (2)
//     Dokumentieren sie kurz, wie ihr Program zu kompilieren und zu benutzen ist und welche Ergebnisse sie erhalten haben. (1)

@SuppressWarnings({"Convert2Diamond", "SpellCheckingInspection", "DuplicatedCode", "ConstantConditions"})
public class scheng2s_uebung05_lzw implements PlugInFilter {

    private List<Integer> input, output, lzw;
    private int width, height;
    private long timeEncode, timeDecode;
    private List<DictEntry> dictionary;

    private boolean encodeInfoFlag, decodeInfoFlag, testValuesFlag, resultFlag;
    private int[] testValues;

    public int setup(String arg, ImagePlus img) {
        this.dictionary = new ArrayList<DictEntry>();
        for (int i = 0; i <= 255; i++) this.dictionary.add(new DictEntry(i, i)); // fill 0 - 255
        this.lzw = new ArrayList<Integer>();
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        try {
            showDialogOne();
            if (this.testValuesFlag) { // test mode
                this.width = this.testValues.length;
                this.height = 1;
                encode(this.testValues);
            } else { // real mode
                this.width = ip.getWidth();
                this.height = ip.getHeight();
                if (!isGrey(ip)) ip = colorToGrey(ip);
                encode(imageProToArrayList(ip));
            }
            decode(this.lzw, this.dictionary, this.width, this.height);
        } catch (IOException e) {
            return; // quit
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.resultFlag) printResultInfo();
    }

    private void encode(int[] input) throws Exception {
        List<Integer> intList = new ArrayList<Integer>();
        for (int i : input) intList.add(i);
        encode(intList);
    }

    private void encode(List<Integer> input) throws Exception {
        if (this.encodeInfoFlag) print("ENCODING");
        IJ.showStatus("Encoding...");
        this.input = cloneList(input);
        long millis = System.currentTimeMillis();
        List<Integer> checker = new ArrayList<Integer>();
        int counter = 0, progress = 0;

        while (!input.isEmpty()) {
            IJ.showProgress(progress, this.height * this.width);
            if (this.encodeInfoFlag) {
                drawLine();
                print(counter++ + ".");
                print("input (size:" + input.size() + ") = " + input.toString());
            }
            checker.add(input.get(0));
            input.remove(0);
            progress++; // progress bar
            int lastFoundIndex = -1; // reduce searching, improve performance enormously
            DictEntry entryFinder;

            do {
                checker.add(input.get(0));
                input.remove(0);
                progress++;
                entryFinder = searchDictEntry(checker);
                if (entryFinder != null) lastFoundIndex = entryFinder.getIndex();
                if (input.isEmpty()) break; // hitting end
            } while (entryFinder != null);

            if (!input.isEmpty()) {
                int lastItem = checker.get(checker.size() - 1);
                List<Integer> findChain = cloneList(checker);
                findChain.remove(checker.size() - 1); // remove last item
                if (lastFoundIndex == -1) addToLZW(searchDictEntry(findChain).getIndex());
                else addToLZW(lastFoundIndex);
                input.add(0, lastItem);
                progress--;
                addToDict(checker);
                checker.clear(); // reset
            } else { // last step
                if (lastFoundIndex != -1)
                    addToLZW(lastFoundIndex);
                else {
                    List<Integer> findChain = cloneList(checker);
                    findChain.remove(checker.size() - 1); // remove last item
                    addToLZW(searchDictEntry(findChain).getIndex());
                }
                if (entryFinder == null) addToLZW(checker.get(checker.size() - 1)); // add last value
            }
            if (this.encodeInfoFlag) {
                print(dictToString());
                print(lzwToString());
            }
        }
        if (!input.isEmpty()) throw new Exception("Something went wrong in encode(): !input.isEmpty()");
        millis = System.currentTimeMillis() - millis;
        this.timeEncode = millis;
        if (this.encodeInfoFlag) {
            drawLine();
            print("encoding finished in " + millis + " ms");
            drawLine();
        }
    }

    private void decode(List<Integer> lzw, List<DictEntry> dictionary, int width, int height) throws Exception {
        long millis = System.currentTimeMillis();
        if (this.decodeInfoFlag) print("DECODING");
        IJ.showStatus("Decoding...");
        List<Integer> output = new ArrayList<Integer>();
        for (Integer integer : lzw) output.addAll(dictionary.get(integer).getValues());
        this.output = output;
        if (this.decodeInfoFlag) print("output (size:" + output.size() + ") = " + output.toString());

        if (output.size() != (height * width))
            throw new Exception("Something went wrong: Not enough pixel to fill picture.");
        if (!this.input.equals(output))
            throw new Exception("Something went wrong: input != output");

        ImageProcessor ip = concreteDecode(cloneList(output));
        millis = System.currentTimeMillis() - millis;
        new ImagePlus("Decode: " + millis + " ms", ip).show();

        long millis2 = System.currentTimeMillis();
        ImageProcessor ip2 = concreteDecodeWithColors(ip, cloneList(lzw), dictionary);
        millis2 = System.currentTimeMillis() - millis2;
        new ImagePlus("Colored Decode: " + millis2 + " ms", ip2).show();

        this.timeDecode = millis + millis2;
        if (this.decodeInfoFlag) print("decoding finished in " + this.timeDecode + " ms");
    }

    private ImageProcessor concreteDecode(List<Integer> values) {
        ImageProcessor result = new ColorProcessor(this.width, this.height);
        for (int h = 0; h < this.height; h++) {
            IJ.showProgress(h, this.height);
            for (int w = 0; w < this.width; w++) {
                int grey = values.get(0);
                values.remove(0);
                result.putPixel(w, h, new int[]{grey, grey, grey});
            }
        }
        return result;
    }

    private ImageProcessor concreteDecodeWithColors(ImageProcessor ip, List<Integer> lzw, List<DictEntry> dictionary) {
        ImageProcessor result = new ColorProcessor(this.width, this.height);
        int[] indexStack = new int[this.width * this.height], rgb = new int[3];
        int counter = 0, color = 2, lastIndex = -1; // color starts with red
        for (Integer integer : lzw)
            for (int j = 0; j < dictionary.get(integer).size(); j++)
                indexStack[counter++] = dictionary.get(integer).getIndex();
        int[][] colorSwitcher = {{255, 0, 0}, {0, 255, 0}, {0, 0, 255}}; // r,g,b
        counter = 0; // reset counter
        for (int h = 0; h < this.height; h++) {
            IJ.showProgress(h, this.height);
            for (int w = 0; w < this.width; w++) {
                ip.getPixel(w, h, rgb);
                int grey = rgb[0], index = indexStack[counter++];
                if (lastIndex != index) color = ++color % colorSwitcher.length; // change color
                int r = (int) (0.2 * grey + 0.8 * colorSwitcher[color][0]);
                int g = (int) (0.2 * grey + 0.8 * colorSwitcher[color][1]);
                int b = (int) (0.2 * grey + 0.8 * colorSwitcher[color][2]);
                result.putPixel(w, h, new int[]{r, g, b});
                lastIndex = index;
            }
        }
        return result;
    }

    // lzw helper

    private void addToLZW(int index) {
        this.lzw.add(index);
        if (this.encodeInfoFlag) print("new lzw entry = " + this.dictionary.get(index).print());
    }

    // dict helper

    private void addToDict(List<Integer> values) {
        this.dictionary.add(new DictEntry(this.dictionary.size(), cloneList(values)));
        if (this.encodeInfoFlag) print("new dictionary entry = " + values.toString());
    }

    private DictEntry searchDictEntry(List<Integer> values) {
        if (values.size() == 1) {
            if (this.encodeInfoFlag)
                print("search dictionary entry: " + values.toString() + " found: #" + values.get(0));
            return this.dictionary.get(values.get(0)); // only one value: value = index
        }
        for (int i = this.dictionary.size() - 1; i > 255; i--) { // reverse search: bigger first
            DictEntry dictEntry = this.dictionary.get(i);
            if (dictEntry.compare(values)) {
                if (this.encodeInfoFlag)
                    print("search dictionary entry: " + values.toString() + " found: #" + dictEntry.getIndex());
                return dictEntry;
            }
        }
        if (this.encodeInfoFlag) print("search dictionary entry: " + values.toString() + " not found");
        return null; // not found
    }

    // other helper

    private List<Integer> cloneList(List<Integer> old) {
        ArrayList<Integer> copy = new ArrayList<Integer>(old.size());
        copy.addAll(old);
        return copy;
    }

    List<Integer> imageProToArrayList(ImageProcessor ip) {
        List<Integer> list = new ArrayList<Integer>();
        int[] rgb = new int[3];
        for (int h = 0; h < this.height; h++)
            for (int w = 0; w < this.width; w++) {
                ip.getPixel(w, h, rgb);
                list.add(rgb[0]);
            }
        return list;
    }

    private boolean isGrey(ImageProcessor ip) {
        int[] rgb = new int[3];
        for (int x = 0; x < this.width; x++)
            for (int y = 0; y < this.height; y++) {
                ip.getPixel(x, y, rgb);
                if (rgb[0] != rgb[1] || rgb[1] != rgb[2]) return false;
            }
        return true;
    }

    private ImageProcessor colorToGrey(ImageProcessor ip) {
        ImageProcessor greyIP = ip.createProcessor(this.width, this.height);
        int[] rgb = new int[3];
        for (int x = 0; x < this.width; x++)
            for (int y = 0; y < this.height; y++) {
                ip.getPixel(x, y, rgb);
                int grey = (int) (0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2]);
                greyIP.putPixel(x, y, new int[]{grey, grey, grey});
            }
        return greyIP;
    }

    // printer

    @SuppressWarnings("FieldCanBeLocal")
    private boolean printIDE = false;

    private void print(String info) {
        if (this.printIDE) System.out.println(info);
        else IJ.log(info);
    }

    private void drawLine() {
        print("-------------------------------------------------------------------------------");
    }

    private String lzwToString() {
        StringBuilder s = new StringBuilder("[");
        for (Integer integer : this.lzw) {
            if (integer > 255) s.append("#"); // index
            s.append(integer).append(", ");
        }
        return "lzw (size:" + this.lzw.size() + ") = " + s.substring(0, s.length() - 2) + "]";
    }

    private String dictToString() {
        if (this.dictionary.size() == 256) return "dictionary (size:256) = default values";
        StringBuilder s = new StringBuilder();
        for (int i = 256; i < this.dictionary.size(); i++) s.append(this.dictionary.get(i).print()).append(", ");
        return "dictionary (size:" + this.dictionary.size() + ") = " + s.substring(0, s.length() - 2);
    }

    private void printResultInfo() {
        drawLine();
        print("RESULT");
        print("input (size:" + this.input.size() + ") = " + this.input.toString());
        print(lzwToString());
        print(dictToString());
        print("output (size:" + this.output.size() + ") = " + this.output.toString());
        print("encoding time = " + this.timeEncode + " ms");
        print("decoding time = " + this.timeDecode + " ms");
    }

    // dialog

    private void showDialogOne() throws IOException {
        IJ.showStatus("Status: Waiting");
        GenericDialog gd = new GenericDialog("Lempel–Ziv–Welch Algorithm by Siu Cheng");
        String[] item = {"Yes    ", "No"};
        gd.addRadioButtonGroup("Print Result Information:", item, 1, item.length, item[0]);
        gd.addRadioButtonGroup("Print Encode Information:", item, 1, item.length, item[1]);
        gd.addRadioButtonGroup("Print Decode Information:", item, 1, item.length, item[1]);
        gd.addRadioButtonGroup("Use Test Values:", item, 1, item.length, item[1]);
        gd.showDialog();
        if (gd.wasCanceled()) throw new IOException("Cancel");
        this.resultFlag = gd.getNextRadioButton().startsWith("Y");
        this.encodeInfoFlag = gd.getNextRadioButton().startsWith("Y");
        this.decodeInfoFlag = gd.getNextRadioButton().startsWith("Y");
        this.testValuesFlag = gd.getNextRadioButton().startsWith("Y");
        if (this.testValuesFlag) showDialogTwo();
    }

    private void showDialogTwo() throws IOException {
        GenericDialog gd = new GenericDialog("Lempel–Ziv–Welch Algorithm by Siu Cheng");
        String[] item = {"Yes    ", "No"};
        gd.addStringField("Test Values:", "1,2,3,1,3,4,1,2,3,1,2,3", 100);
        gd.addRadioButtonGroup("Print Result Information:", item, 1, item.length, item[0]);
        gd.addRadioButtonGroup("Print Encode Information:", item, 1, item.length, item[0]);
        gd.addRadioButtonGroup("Print Decode Information:", item, 1, item.length, item[0]);
        gd.showDialog();
        if (gd.wasCanceled()) throw new IOException("Cancel");
        String input = gd.getNextString();
        this.resultFlag = gd.getNextRadioButton().startsWith("Y");
        this.encodeInfoFlag = gd.getNextRadioButton().startsWith("Y");
        this.decodeInfoFlag = gd.getNextRadioButton().startsWith("Y");
        try {
            input = input.replaceAll("[^0-9,]", "").replaceAll(",+", ",");
            int[] arr = new int[10];
            int count = 0;
            String[] split = input.split(",");
            for (String s : split) {
                int i = Integer.parseInt(s);
                if (arr.length == count) arr = Arrays.copyOf(arr, count * 2);
                arr[count++] = i;
            }
            this.testValues = Arrays.copyOfRange(arr, 0, count);
            if (this.testValues.length < 2) throw new IOException();
        } catch (Exception e) {
            IJ.error("Error", "Something went wrong. Please check your input and try again. Please enter at least two number." +
                    "\nAllowed characters: numerics and \",\"");
            showDialogTwo(); // restart
        }
    }

    static class DictEntry {
        private int index; // incrementint number: #1, #2 ...
        private List<Integer> values; // greyscale values: 0-255
        private Integer size;

        DictEntry(int index, int value) {
            this.index = index;
            this.values = new ArrayList<Integer>();
            this.values.add(value);
        }

        DictEntry(int index, List<Integer> values) {
            this.index = index;
            this.values = values;
        }

        public boolean compare(List<Integer> input) {
            if (size() != input.size()) return false; // probably faster than equals
            else return getValues().equals(input);
        }

        public int size() {
            if (this.size == null) this.size = getValues().size(); // lazy
            return this.size;
        }

        public String print() {
            return "#" + getIndex() + ": " + getValues().toString();
        }

        // getter

        public int getIndex() {
            return this.index;
        }

        public List<Integer> getValues() {
            return this.values;
        }
    }
}