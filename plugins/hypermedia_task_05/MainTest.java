package plugins.hypermedia_task_05;

import java.util.ArrayList;
import java.util.List;

public class MainTest {

    private static int width = 10;
    private static int height = 10;


    private static void encrypt(List<Integer> lastValues, int w, int h) {
        System.out.print("w = " + w + ", h = " + h + ", ");
        int[] rgb = new int[3];
        int currentValue = rgb[0];
        int[] nextPos = nextPos(w, h);

        if (w == 0 && h == 0) { // start
            System.out.println("first pixel");
            // sourceIP.getPixel(w, h, rgb);
            encrypt(lastValues, nextPos[0], nextPos[1]);
        } else if (nextPos == null) { // last
            System.out.println("last pixel");
//            DictEntry dictEntry = searchDictEntry(lastValues);
//            if (dictEntry == null) { // new value
//                int index = dict.size();
//                dict.add(new DictEntry(index, lastValues));
//                lzw.add(index);
//            } else // existing value
//                lzw.add(dictEntry.getIndex());
        } else {
            System.out.println("between pixel");
            encrypt(lastValues, nextPos[0], nextPos[1]);
        }
    }

    // coordinate helper

    static int[] prevPos(int w, int h) {
        return w == 0 && h == 0 ? null : (--w == -1) ? new int[]{0, --h} : new int[]{w, h};

    }

    private static int[] nextPos(int w, int h) {
        return w == width - 1 && h == height - 1 ? null : (++w == width) ? new int[]{0, ++h} : new int[]{w, h};
    }


    public static void main(String[] args) {
//        for (int h = 0; h < height; h++) {
//            for (int w = 0; w < width; w++) {
//                int[] wh = nextPos(w, h);
//                System.out.println("w = " + w);
//                System.out.println("h = " + h);
//                if (wh != null)
//                    System.out.println("{w=" + wh[0] + ",h=" + wh[1] + "}");
//                else
//                    System.out.println("END");
//                System.out.println();
//            }
//        }
        System.out.println("width = " + width);
        System.out.println("height = " + height);
//        for (int h = height - 1; h >= 0; h--) {
//            for (int w = width - 1; w >= 0; w--) {
//                int[] wh = prevPos(w, h);
//                System.out.println("w = " + w);
//                System.out.println("h = " + h);
//                if (wh != null)
//                    System.out.println("{w=" + wh[0] + ",h=" + wh[1] + "}");
//                else
//                    System.out.println("END");
//                System.out.println();
//            }
//        }
        List<Integer> tmp = new ArrayList<Integer>();
        tmp.add(-1);
        encrypt(tmp, 0, 0);
    }
}
