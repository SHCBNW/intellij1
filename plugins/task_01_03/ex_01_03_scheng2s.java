
import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Aufgabe:
 * 3. Entwickeln Sie ein weiteres ImageJ-Plugin, das die
 * RGB-Kanäle in CMY bzw. CMYK umwandelt und diese neuen
 * Farbkomponenten analog zur RGB(A)-Trennung separiert. D.h.
 * die Kanäle C, M und Y sollen in der jeweiligen Farbe vor
 * weißem Hintergrund dargestellt werden; die K-Komponente
 * von CMYK soll als Graustufenbild dargestellt werden. (3 BP)
 * <p>
 * Umrechnung nach: https://www.rapidtables.com/convert/color/rgb-to-cmyk.html
 */

@SuppressWarnings("WrongPackageStatement")
public class ex_01_03_scheng2s implements PlugInFilter {
    ImagePlus imp;
    ImageProcessor ip;
    boolean seeLogs = true;

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.ip = ip;
        splitCMYK();
    }

    private void splitCMYK() {
        int w = ip.getWidth();
        int h = ip.getHeight();
        int[] rgb = new int[3];
        // Create three new images fur each CMYK
        ImageProcessor c = ip.createProcessor(w, h);
        ImageProcessor m = ip.createProcessor(w, h);
        ImageProcessor y = ip.createProcessor(w, h);
        ImageProcessor k = ip.createProcessor(w, h);
        float fR, fG, fB, fK, fC, fM, fY;
        int iK;
        for (int u = 0; u < w; u++) {
            for (int v = 0; v < h; v++) {
                ip.getPixel(u, v, rgb);

                // Convert RGB 0-255 to 0-1
                fR = rgb[0] / 255.0f;
                fG = rgb[1] / 255.0f;
                fB = rgb[2] / 255.0f;

                // Convert RGB to CMYK
                fK = 1.0f - Math.max(fR, Math.max(fG, fB));
                if (fK >= 1.0f) fC = fM = fY = 0.0f;
                else {
                    fC = (1.0f - fR - fK) / (1.0f - fK);
                    fM = (1.0f - fG - fK) / (1.0f - fK);
                    fY = (1.0f - fB - fK) / (1.0f - fK);
                }

                if (seeLogs && u == 0 && v == 0) {
                    IJ.log("R=" + rgb[0] + "G=" + rgb[1] + "B=" + rgb[2]);
                    IJ.log("K=" + fK);
                    IJ.log("C=" + fC + " M=" + fM + " Y=" + fY);
                }

                // Normalize colors to 255
                c.putPixel(u, v, new int[]{255 - Math.round(fC * 255.0f), 255, 255});
                m.putPixel(u, v, new int[]{255, 255 - Math.round(fM * 255.0f), 255});
                y.putPixel(u, v, new int[]{255, 255, 255 - Math.round(fY * 255.0f)});
                iK = 255 - Math.round(fK * 255.0f);
                k.putPixel(u, v, new int[]{iK, iK, iK});
            }
        }
        // Show new images
        new ImagePlus("cyan", c).show();
        new ImagePlus("magenta", m).show();
        new ImagePlus("yellow", y).show();
        new ImagePlus("black", k).show();
    }
}