
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Task:
 * Generate a noisy copy of a greyscale image by superimposing a RGBA picture with randomly
 * distributed transparent, black, and white pixels (salt & pepper noise). (1 BP)
 */

@SuppressWarnings({"SpellCheckingInspection", "unused", "ConstantConditions", "SillyAssignment", "WrongPackageStatement"})
public class ex_03_02a_scheng2s implements PlugInFilter {
    private ImagePlus imp;
    private ImageProcessor ip;

    public ex_03_02a_scheng2s() {
    }

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.ip = ip;
        showOptionsMenu();
    }

    private void showOptionsMenu() {
        GenericDialog gd = new GenericDialog("Salt And Pepper by Siu Ho Cheng");
        gd.addMessage("Salt And Pepper Intensity:");
        gd.addSlider("%", 1, 100, 50);
        gd.showDialog();
        if (gd.wasCanceled())
            return;
        saltAndPepper(gd.getNextNumber() / 100);
    }

    private void saltAndPepper(double percent) {
        long millis = System.currentTimeMillis();
        if (percent != 0.0) {
            ImageProcessor saltyIp = ip.duplicate();
            int w = ip.getWidth(), h = ip.getHeight();
            for (int i = 0; i < (int) (w * h * percent); i++) {
                saltyIp.putPixel(getRanBetween(0, w), getRanBetween(0, h), new int[]{255, 255, 255});// Put white
                saltyIp.putPixel(getRanBetween(0, w), getRanBetween(0, h), new int[]{0, 0, 0});// Put black
            }
            millis = System.currentTimeMillis() - millis;
            new ImagePlus("Salted picture, " + millis + " ms", saltyIp).show();
        }
        System.out.println(millis + " ms");
    }

    @SuppressWarnings("SameParameterValue")
    private int getRanBetween(int min, int max) {
        return (int) (Math.random() * ((max - min) + 1)) + min;
    }
}