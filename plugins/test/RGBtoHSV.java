package plugins.test;

import java.awt.*;
import java.util.Arrays;

public class RGBtoHSV {

    public static void main(String[] args) {
        rgb2hsv(168, 93, 176);
    }

    private static void rgb2hsv(int r, int g, int b) {
        float[] hsv = new float[3];
        Color.RGBtoHSB(r, g, b, hsv);
        hsv[0] = hsv[0] * 360;
        hsv[1] = hsv[1] * 100;
        hsv[2] = hsv[2] * 100;

        System.out.println(Arrays.toString(hsv));
    }
}
