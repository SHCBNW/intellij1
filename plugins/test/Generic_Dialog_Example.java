package plugins.test;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

public class Generic_Dialog_Example implements PlugInFilter {

    ImagePlus imp;
    ImageProcessor ip;

    @Override
    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    @Override
    public void run(ImageProcessor ip) {
        String title = "Example";
        int width = 512, height = 512;

        GenericDialog gd = new GenericDialog("New Image");
        gd.addStringField("Title: ", title);
        gd.addNumericField("Width: ", width, 0);
        gd.addNumericField("Height: ", height, 0);
        gd.showDialog();


        if (gd.wasCanceled())
            return;
        title = gd.getNextString();
        width = (int) gd.getNextNumber();
        height = (int) gd.getNextNumber();
        IJ.newImage(title, "8-bit", width, height, 1);

    }
}
