package plugins.test;

import ij.gui.GenericDialog;

@SuppressWarnings("Duplicates")
public class GenericDialogMain {

    public static void main(String[] args) {
        String title = "Example";
        int width = 512, height = 512;

        GenericDialog gd = new GenericDialog("New Image");
        String[] s = {"Aufgabe A", "Aufgabe B"};
        gd.addChoice("bla: ", s, s[0]);

//        gd.addStringField("Title: ", title);
//        gd.addNumericField("Width: ", width, 0);
//        gd.addNumericField("Height: ", height, 0);
//
//        boolean b = false;
//        gd.addCheckbox("Check?", b);
//
//
//        String[] s = {"a", "b"};
//        String s2 = "vla";
//        gd.addChoice("bla: ", s, s[0]);
//        gd.addSlider("Slide however", 0.0, 100.0, 0.0);
//        gd.addMessage("BLALLA");
//
//        gd.addTextAreas("1", "2", 3, 2);
//        gd.addHelp("https://imagej.nih.gov/ij/developer/api/ij/gui/GenericDialog.html#addSlider-java.lang.String-double-double-double-");

        gd.showDialog();


        if (gd.wasCanceled())
            return;

        System.out.println(gd.getNextChoiceIndex());

        return;
//        title = gd.getNextString();
//        width = (int) gd.getNextNumber();
//        height = (int) gd.getNextNumber();
//
//        System.out.println(title);
//        System.out.println(width);
//        System.out.println(height);
//        System.out.println(gd.getNextChoiceIndex());
//        System.out.println(gd.getSliders().get(0));
//        IJ.newImage(title, "8-bit", width, height, 1);
    }
}
