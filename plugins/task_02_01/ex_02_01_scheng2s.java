
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Aufgabe:
 * 1. Spreizung + Offset: Wandeln Sie ein kontrastarmes RGB-Bild so in ein 8bit-Graustufenbild um,
 * dass sämtliche Grauwerte im Bereich zwischen 100 und 200 liegen. (1 BP)
 */


@SuppressWarnings({"WeakerAccess", "ConstantConditions", "SillyAssignment", "unused", "WrongPackageStatement"})
public class ex_02_01_scheng2s implements PlugInFilter {
    ImagePlus imp;
    ImageProcessor ip;

    public ex_02_01_scheng2s() {
    }

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.ip = ip;
        colorToGreyscaleWithColorstretching();
    }

    private void colorToGreyscaleWithColorstretching() {
        int w = ip.getWidth();
        int h = ip.getHeight();
        int[] rgb = new int[3];
        for (int u = 0; u < w; u++)
            for (int v = 0; v < h; v++) {
                // Get Colors and put it in rgb[]
                ip.getPixel(u, v, rgb);
                // RGB to grayscale as ex_01_01
                int greyVal = (rgb[0] + rgb[1] + rgb[2]) / 3;
                // 0 - 255 to 100 - 200 with Stretching + Offset
                double iNew = getiNew(greyVal, 0.0, 255.0, 100.0, 200.0);
                // Replace pixel with grayscale values
                ip.putPixel(u, v, new int[]{(int) iNew, (int) iNew, (int) iNew});
            }
    }

    @SuppressWarnings("SameParameterValue")
    public double getiNew(int greyVal, double aLow, double aHigh, double bLow, double bHigh) {
        return bLow + ((bHigh - bLow) / (aHigh - aLow)) * (greyVal - aLow);
    }
}