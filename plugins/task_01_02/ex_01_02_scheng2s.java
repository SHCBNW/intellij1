
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Aufgabe:
 * 2. Erstellen Sie ein ImageJ-Plugin, das die Farbkanäle eines RGBA-Bildes
 * in 3 Einzelbilder zerlegt, welche die Rot-, Grün- und Blau-Kanäle einzeln
 * (in der jeweiligen Farbe) anzeigen. (1 BP)
 */

@SuppressWarnings("WrongPackageStatement")
public class ex_01_02_scheng2s implements PlugInFilter {
    ImagePlus imp;
    ImageProcessor ip;

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.ip = ip;
        splitRGB();
    }

    private void splitRGB() {
        int w = ip.getWidth();
        int h = ip.getHeight();
        int[] rgb = new int[3];
        // Create three new images fur each RGB
        ImageProcessor r = ip.createProcessor(w, h);
        ImageProcessor g = ip.createProcessor(w, h);
        ImageProcessor b = ip.createProcessor(w, h);
        for (int u = 0; u < w; u++) {
            for (int v = 0; v < h; v++) {
                ip.getPixel(u, v, rgb);
                // Overwrite the "useless" colors with 0.
                r.putPixel(u, v, new int[]{rgb[0], 0, 0});
                g.putPixel(u, v, new int[]{0, rgb[1], 0});
                b.putPixel(u, v, new int[]{0, 0, rgb[2]});
            }
        }
        // Show new images
        new ImagePlus("red", r).show();
        new ImagePlus("green", g).show();
        new ImagePlus("blue", b).show();
    }
}