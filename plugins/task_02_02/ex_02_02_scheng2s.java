
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Aufgabe:
 * 2. Spreizung: Spreizen Sie das Ergebnisbild der ersten Teilaufgabe auf die volle
 * 8bit-Bandbreite (0-255). Vergleichen Sie die Histogramme zu beiden Bildern. (1 BP)
 */

@SuppressWarnings({"WeakerAccess", "ConstantConditions", "SillyAssignment", "unused", "WrongPackageStatement"})
public class ex_02_02_scheng2s implements PlugInFilter {
    ImagePlus imp;
    ImageProcessor ip;

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public ex_02_02_scheng2s() {
    }

    public void run(ImageProcessor ip) {
        this.ip = ip;
        colorToGreyscaleWithColorstretching2();
    }

    private void colorToGreyscaleWithColorstretching2() {
        int w = ip.getWidth();
        int h = ip.getHeight();
        int[] rgb = new int[3];
        for (int u = 0; u < w; u++)
            for (int v = 0; v < h; v++) {
                // Get Colors and put it in rgb[]
                ip.getPixel(u, v, rgb);
                // RGB to grayscale as ex_01_01
                int greyVal = (rgb[0] + rgb[1] + rgb[2]) / 3;
                // 0 - 255 to 100 - 200
                double iNew = getiNew(greyVal, 0.0, 255.0, 100.0, 200.0);
                // 100 - 200 to 0 - 255
                double iNew2 = getiNew((int) iNew, 100.0, 200.0, 0.0, 255.0);
                // Replace pixel with grayscale values
                ip.putPixel(u, v, new int[]{(int) iNew2, (int) iNew2, (int) iNew2});
            }
    }

    private double getiNew(int greyVal, double aLow, double aHigh, double bLow, double bHigh) {
        return bLow + ((bHigh - bLow) / (aHigh - aLow)) * (greyVal - aLow);
    }
}