// package plugins.task_04_01;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

import java.io.IOException;
import java.text.MessageFormat;

/**
 * Task:
 * Choose (or generate) a sample picture showing several objects differing in brightness with distinct edges in
 * front of a distinguishable background. Choose objects and background in a way that a single binarisation threshold
 * for the entire image would not preserve all objects correctly.
 * Threshold values: Transform the picture to a binary image such that all objects are visible, using appropriate
 * (e.g. local adaptive) thresholds. (2 BP)
 * Edges: Generate a picture from the binary image in a way that only the edges of the objects are visible, using
 * morphological operators. (3  BP)
 * <p>
 * Perform the above named tasks with your own ImageJ plugins and report your work.
 */

@SuppressWarnings({"ConstantConditions", "WrongPackageStatement", "unused", "SillyAssignment", "SpellCheckingInspection", "FieldCanBeLocal"})
public class ex_04_scheng2s implements PlugInFilter {
    private ImagePlus imp;
    private ImageProcessor originalImage;
    private Task1 task1;
    private Task2 task2;
    private Task3 task3;
    private int width, height;
    private boolean task1Flag, task2Flag, task3Flag;
    private final int[] RGB_WHITE = {255, 255, 255}, RGB_BLACK = {0, 0, 0};
    private final int BINARY_WHITE = 0, BINARY_BLACK = 1;

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.originalImage = ip;
        this.width = originalImage.getWidth();
        this.height = originalImage.getHeight();

        try {
            showOptionsMenu();
            if (task1Flag) {
                this.task1 = new Task1();
                task1.run(originalImage);
            }
            if (task2Flag) {
                this.task2 = new Task2();
                if (task1Flag)
                    task2.run(task1.newImage);
                else
                    task2.run(originalImage);
            }
            if (task3Flag) {
                this.task3 = new Task3();
                task3.run(task2.newImage);
            }
        } catch (IOException e) {
            //noinspection UnnecessaryReturnStatement
            return;
        }
    }

    private void showOptionsMenu() throws IOException {
        GenericDialog gd = new GenericDialog("Exercise 04 by Siu Ho Cheng");
        String[] items = {"Task 1", "Task 2", "Task 1 + Task 2", "Task 1 + Task 2 + Task 3"};
        gd.addRadioButtonGroup("Select:", items, items.length, 1, items[0]);
        gd.showDialog();
        if (gd.wasCanceled())
            throw new IOException("Cancel");
        String input = gd.getNextRadioButton();
        task1Flag = input.contains("1");
        task2Flag = input.contains("2");
        task3Flag = input.contains("3");
    }

    private class Task1 {
        private int saturationThreshold, brightnessThreshold;
        private ImageProcessor inputImage, newImage;

        private void run(ImageProcessor ip) throws IOException {
            this.inputImage = ip;
            showOptionsMenu();
            long millis = System.currentTimeMillis();
            RGBtoBinary();
            new ImagePlus(
                    MessageFormat.format( // Prints information in window titlebar
                            "Binary picture, S = {0}, B = {1}, {2} ms",
                            this.saturationThreshold, this.brightnessThreshold, System.currentTimeMillis() - millis),
                    newImage
            ).show();
        }

        private void showOptionsMenu() throws IOException {
            GenericDialog gd = new GenericDialog("Binary Filter with HSB by Siu Ho Cheng");

            // Items
            String[] item = {"a) Saturation + Brightness", "b) Saturation Only", "c) Brightness Only"};
            gd.addRadioButtonGroup("Please select detection mode:", item, 3, 1, item[0]);
            gd.addSlider("Saturation Threshold:", 0, 100, 50);
            gd.addSlider("Brightness Threshold", 0, 100, 50);
            gd.showDialog();
            if (gd.wasCanceled()) throw new IOException("Cancel");

            // Inputs
            String modeTmp = gd.getNextRadioButton().substring(0, 1);
            byte mode = (byte) (modeTmp.equals("a") ? 0 : modeTmp.equals("b") ? 1 : 2);
            this.saturationThreshold = (int) gd.getNextNumber();
            this.brightnessThreshold = (int) gd.getNextNumber();
            this.saturationThreshold = mode == 2 ? 0 : this.saturationThreshold;
            this.brightnessThreshold = mode == 1 ? 0 : this.brightnessThreshold;
            if (this.saturationThreshold < 0 || this.saturationThreshold > 100 ||
                    this.brightnessThreshold < 0 || this.brightnessThreshold > 100) { // if anything OutofBounce
                IJ.error("Error Value out of bounce.",
                        "Please select saturation and brightness values between 0 and 100. Click OK to retry.");
                showOptionsMenu(); // Restart showOptionsMenu();
            }
        }

        private void RGBtoBinary() {
            int[] rgb = new int[3];
            int binaryVal;
            newImage = inputImage.createProcessor(width, height);
            for (int picX = 0; picX < width; picX++)
                for (int picY = 0; picY < height; picY++) {
                    inputImage.getPixel(picX, picY, rgb); // Get RGB
                    newImage.putPixel(picX, picY, binaryDecider(rgb)); // Put Binary
                }
        }

        private int[] binaryDecider(int[] rgb) {
            double[] sb = RGBtoSB(rgb[0], rgb[1], rgb[2]); // All values between 0.0 and 1.0
            // saturation check
            if (sb[0] >= (100 - this.saturationThreshold) / 100.0) // Inverted for better usability in UI: Move slider to right to see more.
                return RGB_BLACK; // black
            // value check
            if (sb[1] < this.brightnessThreshold / 100.0)
                return RGB_BLACK; // black
            return RGB_WHITE; // white
        }

        private double[] RGBtoSB(int r, int g, int b) { // Source: java\awt\Color.java (minimized, without hue)
            double saturation;
            int cmax = (r > g) ? r : g;
            if (b > cmax) cmax = b;
            int cmin = (r < g) ? r : g;
            if (b < cmin) cmin = b;
            if (cmax != 0)
                saturation = ((double) (cmax - cmin)) / ((double) cmax);
            else
                saturation = 0;
            return new double[]{saturation, cmax / 255.0};
        }
    }

    // TASK 2
    private class Task2 {
        private ImageProcessor inputImage, newImage;
        private StructuralElement structElement;
        private boolean rotate90Flag, rotate180Flag;

        private void run(ImageProcessor ip) throws IOException {
            this.inputImage = ip;
            showOptionsMenu();
            long millis = System.currentTimeMillis();
            this.newImage = this.inputImage.createProcessor(width, height);
            // Fill newImage with white
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    this.newImage.putPixel(i, j, RGB_WHITE);
            coordinateHelper();
            if (rotate90Flag)
                for (int i = 0; i < 3; i++) {
                    this.structElement.rotate90();
                    coordinateHelper();
                }
            else if (rotate180Flag) {
                this.structElement.rotate180();
                coordinateHelper();
            }
            new ImagePlus(MessageFormat.format("Morphological Operator, time: {0} ms",
                    System.currentTimeMillis() - millis), this.newImage).show();
        }

        private void showOptionsMenu() throws IOException {
            // Select size
            GenericDialog gdSizes = new GenericDialog("Stuct Element Options (1/3)");
            gdSizes.addMessage("Please select the stuct element size:");
            String[] sizeItems = new String[10];
            for (int i = 0; i < sizeItems.length; i++) sizeItems[i] = String.valueOf(i * 2 + 1);
            gdSizes.addChoice("Width:", sizeItems, sizeItems[1]);
            gdSizes.addToSameRow();
            gdSizes.addChoice("  Height:", sizeItems, sizeItems[0]);
            gdSizes.showDialog();
            if (gdSizes.wasCanceled()) throw new IOException("Cancel");

            byte strctW = Byte.parseByte(sizeItems[gdSizes.getNextChoiceIndex()]);
            byte strctH = Byte.parseByte(sizeItems[gdSizes.getNextChoiceIndex()]);

            if (strctW * strctH < 3) {
                IJ.error("Size too small", "Please select a bigger size.");
                showOptionsMenu();
                return;
            }
            // Select elements
            GenericDialog gdElements = new GenericDialog("Stuct Element Options (2/3)");
            String[] itemsChoice = {"null", "black (1)", "white (0)"};
            for (int i = 0; i < strctW; i++)
                for (int j = 0; j < strctH; j++) {
                    gdElements.addChoice("", itemsChoice, itemsChoice[0]);
                    if (j < strctW - 1) gdElements.addToSameRow();
                }
            gdElements.showDialog();
            if (gdElements.wasCanceled()) throw new IOException("Cancel");

            Integer[][] structElements = new Integer[strctW][strctH];
            for (int i = 0; i < strctW; i++)
                for (int j = 0; j < strctH; j++) {
                    int input = gdElements.getNextChoiceIndex();
                    structElements[i][j] = input == 0 ? null : input == 1 ? 1 : 0;
                }

            // Select rotation
            GenericDialog gdRotation = new GenericDialog("Stuct Element Options (3/3)");

            String[] item = {"a) No Rotation", "b) Yes, 4x with 90° rotations", "c) Yes, 2x with 180° rotation"};
            gdRotation.addRadioButtonGroup("Do you like to rotate your StructuralElement and reuse it?",
                    item, 3, 1, item[0]);
            gdRotation.showDialog();
            if (gdElements.wasCanceled()) throw new IOException("Cancel");
            String rotationMode = gdRotation.getNextRadioButton().substring(0, 1);
            if (rotationMode.equals("b")) {
                this.rotate90Flag = true;
                this.rotate180Flag = false;
            } else if (rotationMode.equals("c")) {
                this.rotate90Flag = false;
                this.rotate180Flag = true;
            } else {
                this.rotate90Flag = false;
                this.rotate180Flag = false;
            }
            this.structElement = new StructuralElement(structElements);
        }

        private void coordinateHelper() {
            int structW = this.structElement.getWidth();
            int structH = this.structElement.getHeight();
            for (int picX = structW / 2; picX < width - structW / 2; picX++)
                for (int picY = structH / 2; picY < height - structH / 2; picY++) {
                    boolean match = this.structElement.compare(picX - structW / 2, picY - structH / 2);
                    if (match) newImage.putPixel(picX, picY, RGB_BLACK);
                }
        }

        class StructuralElement {
            private Integer[][] elements;

            StructuralElement(Integer[][] elements) {
                this.elements = elements;
            }

            private boolean compare(int upLeftX, int upLeftY) {
                int[] rgb = new int[3];
                for (int x = 0; x < getWidth(); x++)
                    for (int y = 0; y < getHeight(); y++)
                        if (this.elements[x][y] != null) {
                            inputImage.getPixel(upLeftX + x, upLeftY + y, rgb); // Get RGB
                            int binaryVal = rgb[0] == 0 ? 1 : 0;
                            if (this.elements[x][y] != binaryVal)
                                return false;
                        }
                return true;
            }

            @SuppressWarnings("UnusedReturnValue")
            private StructuralElement rotate90() {
                Integer[][] newArray = new Integer[getHeight()][getWidth()];
                for (int x = 0; x < getWidth(); x++)
                    for (int y = 0; y < getHeight(); y++)
                        newArray[y][getWidth() - 1 - x] = this.elements[x][y];
                this.elements = newArray;
                return this;
            }

            @SuppressWarnings("UnusedReturnValue")
            private StructuralElement rotate180() {
                rotate90();
                rotate90();
                return this;
            }

            // Getter
            private int getWidth() {
                return elements.length;
            }

            private int getHeight() {
                return elements[0].length;
            }
        }
    }

    @SuppressWarnings("FieldCanBeLocal")
    private class Task3 {
        private ImageProcessor BinaryIP;
        private ImageProcessor newImage;

        public void run(ImageProcessor BinaryIP) {
            this.BinaryIP = BinaryIP;
            long millis = System.currentTimeMillis();
            this.newImage = this.BinaryIP.createProcessor(width, height);
            int[] rgbBinary = new int[3];
            int[] rgbColor = new int[3];

            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++) {
                    BinaryIP.getPixel(i, j, rgbBinary);
                    if (rgbBinary[0] == 0) { // if black478
                        int[] colVal = originalImage.getPixel(i, j, rgbColor);
                        this.newImage.putPixel(i, j, new int[]{colVal[0], colVal[1], colVal[2]});
                    } else
                        this.newImage.putPixel(i, j, RGB_WHITE);
                }
            new ImagePlus(MessageFormat.format("Difference Picture, time: {0} ms",
                    System.currentTimeMillis() - millis), this.newImage).show();
        }
    }
}