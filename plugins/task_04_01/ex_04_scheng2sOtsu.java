import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

import java.io.IOException;

/**
 * Task:
 * Choose (or generate) a sample picture showing several objects differing in brightness with distinct edges in
 * front of a distinguishable background. Choose objects and background in a way that a single binarisation threshold
 * for the entire image would not preserve all objects correctly.
 * Threshold values: Transform the picture to a binary image such that all objects are visible, using appropriate
 * (e.g. local adaptive) thresholds. (2 BP)}
 * Edges: Generate a picture from the binary image in a way that only the edges of the objects are visible, using
 * morphological operators. (3  BP)
 * <p>
 * Perform the above named tasks with your own ImageJ plugins and report your work.
 */

@SuppressWarnings({"WrongPackageStatement", "unused", "SpellCheckingInspection", "FieldCanBeLocal", "Duplicates"})
public class ex_04_scheng2sOtsu implements PlugInFilter {

    private ImageProcessor inputPicture, binaryPicture;
    private int width, height, filterSizeX, filterSizeY;
    private final int[] RGB_WHITE = {255, 255, 255}, RGB_BLACK = {0, 0, 0};
    boolean globalFlag;

    @Override
    public int setup(String arg, ImagePlus imp) {
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        try {
            showOptionsMenu();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        long millis = System.currentTimeMillis();

        this.inputPicture = toGreyScale(ip);
        this.width = ip.getWidth();
        this.height = ip.getHeight();
        this.binaryPicture = ip.createProcessor(width, height);

        int threshold = 0;
        if (globalFlag) {
            threshold = getGlobalOtsuThreshold(this.inputPicture);
            System.out.println(threshold);
        }

        int[] convGrey = new int[3];

        int halfSizeFilterX = this.filterSizeX / 2;
        int halfSizeFilterY = this.filterSizeY / 2;

        System.out.println(halfSizeFilterX);
        System.out.println(halfSizeFilterY);

        for (int pictureX = 0; pictureX < width; pictureX++)
            for (int pictureY = 0; pictureY < height; pictureY++) {
                this.inputPicture.getPixel(pictureX, pictureY, convGrey);
                if (!globalFlag) { // local detection
                    int startX, startY, endX, endY;
                    startX = checkRange(pictureX - halfSizeFilterX, width - 1);
                    startY = checkRange(pictureY - halfSizeFilterY, height - 1);
                    endX = checkRange(pictureX + halfSizeFilterX, width - 1);
                    endY = checkRange(pictureY + halfSizeFilterY, height - 1);
                    threshold = getOtsuThreshold(ip, startX, startY, endX, endY);
//                    System.out.println("" +
//                            "Check (" + pictureX + "," + pictureY + ")" +
//                            "Start (" + startX + "," + startY + ")" +
//                            "End (" + endX + "," + endY + ")" +
//                            "Threshold (" + threshold + ")"
//                    );
                }
                if (convGrey[0] > threshold)
                    binaryPicture.putPixel(pictureX, pictureY, RGB_WHITE);
                else
                    binaryPicture.putPixel(pictureX, pictureY, RGB_BLACK);
            }
        millis = System.currentTimeMillis() - millis;
        new ImagePlus("Binary Picture by Otsu Filter, " + millis + " ms", binaryPicture).show();
    }

    private void showOptionsMenu() throws IOException {
        GenericDialog gd = new GenericDialog("Convolution Filter and Otsu by Siu Ho Cheng");
        gd.addMessage("Please choose the size of the Convolution-Filter:");
        String[] item = {"Global", "Local (Experimental)"};
        gd.addRadioButtonGroup("Mode", item, 2, 1, item[0]);
        gd.addSlider("X", 3, 20, 3);
        gd.addSlider("Y", 3, 20, 3);
        gd.showDialog();
        if (gd.wasCanceled()) throw new IOException("Cancel");
        this.filterSizeX = (int) gd.getNextNumber();
        this.filterSizeY = (int) gd.getNextNumber();
        this.globalFlag = gd.getNextRadioButton().startsWith("Global");
    }

    private ImageProcessor toGreyScale(ImageProcessor colorIP) {
        ImageProcessor greyIP = colorIP.createProcessor(colorIP.getWidth(), colorIP.getHeight());
        int[] rgb = new int[3];
        for (int i = 0; i < colorIP.getWidth(); i++)
            for (int j = 0; j < colorIP.getHeight(); j++) {
                colorIP.getPixel(i, j, rgb);
                int grey = (int) (0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2]);
                greyIP.putPixel(i, j, new int[]{grey, grey, grey});
            }
        // new ImagePlus("grey", greyIP).show();
        return greyIP;
    }

    private int checkRange(int value, int max) {
        return checkRange(value, 0, max);
    }

    @SuppressWarnings("SameParameterValue")
    private int checkRange(int value, int min, int max) {
        return (value < min) ? min : (value > max) ? max : value;
    }

    // Otsu starts here

    private int getGlobalOtsuThreshold(ImageProcessor ip) {
        return getOtsuThreshold(ip, 0, 0, ip.getWidth(), ip.getHeight());
    }

    private int getOtsuThreshold(ImageProcessor ip, int startX, int startY, int endX, int endY) {
        int width = ip.getWidth();
        int height = ip.getHeight();
        // Generate OtsuHistogram
        int[] histogram = OtsuHistogram(ip, startX, startY, endX, endY);
        // Run Otsu
        return calcOtsuThreshold(histogram, width * height);
    }

    private int[] OtsuHistogram(ImageProcessor ip, int startX, int startY, int endX, int endY) {
        int[] greyscale = new int[3];
        int[] histogram = new int[256]; // 0 - 255
        for (int x = startX; x <= endX; x++)
            for (int y = startY; y <= endY; y++) {
                ip.getPixel(x, y, greyscale);
                histogram[greyscale[0]]++;
            }
        return histogram;
    }

    private int sumHisto(int[] histogram) {
        int sum = 0;
        for (int i = 0; i < histogram.length; i++)
            sum += histogram[i] * i;
        return sum;
    }

    private int calcOtsuThreshold(int[] histogram, int total) {
        int sumHisto = sumHisto(histogram), wB = 0, wF, threshold = 0;
        double sumB = 0, varMax = 0;
        for (int i = 0; i < 256; i++) {
            wB += histogram[i];
            if (wB == 0) continue;
            wF = total - wB;
            if (wF == 0) break;
            sumB += (double) (histogram[i] * i);
            double mB = sumB / wB;
            double mF = (sumHisto - sumB) / wF;
            double varNew = (double) wB * (double) wF * (mB - mF) * (mB - mF);
            if (varNew > varMax) {
                varMax = varNew;
                threshold = i;
            }
        }
        return threshold;
    }
}