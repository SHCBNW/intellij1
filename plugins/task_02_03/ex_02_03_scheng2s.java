
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Aufgabe:
 * 3. Falschfarben: Erweitern Sie die Kontrastdarstellung durch eine Falschfarbendarstellung
 * im 24bit-RGB-Farbraum; wählen Sie die Farbskala geeignet für ein intuitives Kontrastverständnis.
 * Vergleichen Sie den sichtbaren Kontrast in Problembereichen zwischen dem gespreizten Graustufenbild
 * und dem Falschfarbenbild. (3 BP)
 * <p>
 * 8bit R + 8bit G + 8bit B = 24 Bit
 * =
 * 256 R + 256 G + 256 B
 * =
 * 256^3 = 16.777.216 colors in total
 */


@SuppressWarnings({"ConstantConditions", "WeakerAccess", "SillyAssignment", "unused", "SpellCheckingInspection", "WrongPackageStatement"})
public class ex_02_03_scheng2s implements PlugInFilter {
    ImagePlus imp;
    ImageProcessor ip;

    public ex_02_03_scheng2s() {
    }

    // H: 0-360, S: 0-100, V: 0-100
    // Return values tested with: https://www.rapidtables.com/web/color/RGB_Color.html
    public static int[] hsvToRgb(double hue, double satuartion, double value) {
        hue /= 360.0;
        satuartion /= 100.0;
        value /= 100.0;
        if (satuartion == 0.0) {
            int tmp = (int) Math.round(value * 255.0);
            return new int[]{tmp, tmp, (tmp)};
        } else {
            double tmpHue = hue * 6.0;
            if (tmpHue == 6.0) tmpHue = 0.0;
            int i = (int) Math.floor(tmpHue);
            double tmp1 = value * (1.0 - satuartion);
            double tmp2 = value * (1.0 - satuartion * (tmpHue - i));
            double tmp3 = value * (1.0 - satuartion * (1 - (tmpHue - i)));
            double r, g, b;
            if (i == 0) {
                r = value;
                g = tmp3;
                b = tmp1;
            } else if (i == 1) {
                r = tmp2;
                g = value;
                b = tmp1;
            } else if (i == 2) {
                r = tmp1;
                g = value;
                b = tmp3;
            } else if (i == 3) {
                r = tmp1;
                g = tmp2;
                b = value;
            } else if (i == 4) {
                r = tmp3;
                g = tmp1;
                b = value;
            } else {
                r = value;
                g = tmp1;
                b = tmp2;
            }
            return new int[]{(int) Math.round(r * 255.0), (int) Math.round(g * 255.0), (int) Math.round(b * 255.0)};
        }
    }


    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.ip = ip;
        colorToGreyscaleToFalseColor();
    }

    private void colorToGreyscaleToFalseColor() {
        int w = ip.getWidth();
        int h = ip.getHeight();
        int[] rgb = new int[3];
        for (int u = 0; u < w; u++)
            for (int v = 0; v < h; v++) {
                // Get Colors and put it in rgb[]
                ip.getPixel(u, v, rgb);
                // RGB to grayscale as ex_01_01
                int greyVal = (rgb[0] + rgb[1] + rgb[2]) / 3;
                // 0 - 255 to 100 - 200
                double iNew = getiNew(greyVal, 0.0, 255.0, 100.0, 200.0);
                // 100 - 200 to 0 - 240 (HSV limit = 360)
                double iNew2 = getiNew((int) iNew, 100.0, 200.0, 0.0, 240.0);
                // h = greyVal, s = 80 (default), v = greyVal, with range between 40.0 - 100.0
                double value = getiNew((int) iNew2, 0.0, 240, 40.0, 100.0);
                int[] newCol = hsvToRgb(240.0 - iNew2, 80.0, value);
                // Replace Pixel with new Colors
                ip.putPixel(u, v, newCol);
            }
    }

    private double getiNew(int greyVal, double aLow, double aHigh, double bLow, double bHigh) {
        return bLow + ((bHigh - bLow) / (aHigh - aLow)) * (greyVal - aLow);
    }
}