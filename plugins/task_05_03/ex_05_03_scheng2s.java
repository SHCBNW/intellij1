
import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// Implementieren Sie ein Skelettierungsverfahren auf der Grundlage des
// Skelettierungs-Algorithmus nach Zhang und Suen als ImageJ-Plugin und wenden Sie dieses
// auf ein geeignetes Beispielbild an. (2 BP)

@SuppressWarnings({"WrongPackageStatement", "unused", "ConstantConditions", "FieldCanBeLocal", "SillyAssignment", "Convert2Diamond", "SpellCheckingInspection"})
public class ex_05_03_scheng2s implements PlugInFilter {
    private ImagePlus imp;
    private ImageProcessor original, result;
    private int width, height;
    private final int RGB_WHITE = 255, RGB_BLACK = 0;
    private final byte BINARY_WHITE = 0, BINARY_BLACK = 1;
    private String titlePrefix = "Zhang-Suen by Cheng";
    private boolean showDifferences, showDebugInformation;

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.width = ip.getWidth();
        this.height = ip.getHeight();
        if (!isBinary(ip)) {
            IJ.error(titlePrefix + ": Error", "This plugin only supports binary picture.");
            return;
        }
        try {
            showDialog();
        } catch (IOException e) {
            return; // quit
        }
        this.original = ip;
        this.result = ip.duplicate();
        zhangAndSuen();
        if (showDifferences)
            showDiff(this.original, this.result);
    }

    public void showDialog() throws IOException {
        IJ.showStatus("Status: Waiting");
        GenericDialog gd = new GenericDialog(titlePrefix);
        String[] item = {"Yes    ", "No"};
        gd.addRadioButtonGroup("Show Differences:", item, 1, item.length, item[0]);
        gd.addRadioButtonGroup("Show Debug Information:", item, 1, item.length, item[1]);
        gd.showDialog();
        if (gd.wasCanceled()) throw new IOException("Cancel");
        this.showDifferences = gd.getNextRadioButton().startsWith("Y");
        this.showDebugInformation = gd.getNextRadioButton().startsWith("Y");
    }

    private void zhangAndSuen() {
        long millis = System.currentTimeMillis();
        boolean changeFlag;
        int loopCounter = -1;
        List<int[]> deleteList = new ArrayList<int[]>();
        ImagePlus resultImagePlus = new ImagePlus(titlePrefix + " - In Progress", this.result);
        resultImagePlus.show();
        do {
            IJ.showStatus("Status: Search and Erase Edges (Loops: " + ++loopCounter + ")");
            changeFlag = false;
            for (int mode = 1; mode <= 2; mode++) {
                for (int x = 0; x < this.width; x++)
                    for (int y = 0; y < this.height; y++)
                        if (subIteration(x, y, mode)) {
                            deleteList.add(new int[]{x, y}); // store delete commands
                            if (showDebugInformation) IJ.log("DELETE!\n");
                        }
                for (int[] coordinates : deleteList) { // perform delete commands
                    changeFlag = true; // apply another loop
                    this.result.putPixel(
                            coordinates[0],
                            coordinates[1],
                            new int[]{RGB_WHITE, RGB_WHITE, RGB_WHITE});
                    resultImagePlus.draw();
                }
                deleteList.clear(); // clear list for next loop
            }
        } while (changeFlag);
        millis = System.currentTimeMillis() - millis;
        resultImagePlus.setTitle(titlePrefix + ", time=" + millis + " ms, loops=" + loopCounter);
        resultImagePlus.draw();
    }

    private boolean subIteration(int x, int y, int mode) {
        if (showDebugInformation) printInfoAtPos(x, y, mode);
        if (p((byte) 1, x, y) == BINARY_WHITE) return false; // if white -> skip
        if (!(a(x, y) == 1)) return false; // condition 1
        byte b = b(x, y);
        if (!(2 <= b && b <= 6)) return false; // condition 2
        byte p2 = p((byte) 2, x, y), p4 = p((byte) 4, x, y), p6 = p((byte) 6, x, y), p8 = p((byte) 8, x, y);
        return mode == 1 ?
                p2 * p4 * p6 == 0 && p4 * p6 * p8 == 0 : // condition 3&4 mode 1
                p2 * p4 * p8 == 0 && p2 * p6 * p8 == 0; // condition 3&4 mode 2
    }

    private byte p(byte pos, int x, int y) { // x and y define the center = p1
        if (pos < 1 || pos > 9) throw new ArrayIndexOutOfBoundsException("1 <= p <= 9");
        x = (pos == 3 || pos == 4 || pos == 5) ? ++x : (pos == 7 || pos == 8 || pos == 9) ? --x : x; // find x coordinates
        y = (pos == 5 || pos == 6 || pos == 7) ? ++y : (pos == 2 || pos == 3 || pos == 9) ? --y : y; // find y coordinates
        return (x < 0 || x > this.width - 1 || y < 0 || y > this.height - 1) ? BINARY_WHITE : // for edges + avoids ArrayOutOfBounce
                (this.result.getPixel(x, y) & 0x0000ff) == RGB_BLACK ? BINARY_BLACK : BINARY_WHITE;
    }

    private byte a(int x, int y) {
        return a((byte) 2, (byte) -1, x, y); // starts with p2 and runs a circle
    }

    private byte a(byte pos, byte prev, int x, int y) {
        if (pos <= 9) { // stop condition
            byte cur = p(pos, x, y); // 0 or 1
            return (byte) (a(++pos, cur, x, y) + (byte) (prev == BINARY_WHITE && cur == BINARY_BLACK ? 1 : 0));
        } else // last step: compare p9 with p2
            return (byte) (prev == BINARY_WHITE && p((byte) 2, x, y) == BINARY_BLACK ? 1 : 0);
    }

    private byte b(int x, int y) {
        return b((byte) 2, x, y); // starts with p2 and runs a circle
    }

    private byte b(byte pos, int x, int y) {
        return (pos <= 9) ? // stop condition
                (byte) (p(pos, x, y) + b(++pos, x, y)) : // current pos + next pos
                0; // last pos
    }

    private void showDiff(ImageProcessor background, ImageProcessor foreground) {
        long millis = System.currentTimeMillis();
        ImageProcessor newImage = background.duplicate();
        ImagePlus resultImagePlus = new ImagePlus(this.titlePrefix + " - Differences", newImage);
        resultImagePlus.show();
        for (int x = 0; x < this.width; x++)
            for (int y = 0; y < this.height; y++)
                if ((foreground.getPixel(x, y) & 0x0000ff) == RGB_BLACK) {
                    newImage.putPixel(x, y, new int[]{0, 255, 0}); // green hardcoded
                    resultImagePlus.draw();
                }
        millis = System.currentTimeMillis() - millis;
        resultImagePlus.setTitle(this.titlePrefix + " - Differences - time=" + millis + " ms");
        resultImagePlus.draw();
    }

    private boolean isBinary(ImageProcessor ip) {
        int[] rgb = new int[3];
        for (int x = 0; x < this.width; x++)
            for (int y = 0; y < this.height; y++) {
                ip.getPixel(x, y, rgb);
                if ((rgb[0] + rgb[1] + rgb[2]) % 765 != 0) return false;
            }
        return true;
    }

    // for debugging
    private void printInfoAtPos(int x, int y, int mode) {
        StringBuilder s = new StringBuilder()
                .append("x, y = ").append(x).append(",").append(y)
                .append(" - Iteration Mode = ").append(mode).append("\n");
        if (p((byte) 1, x, y) == BINARY_WHITE) s.append("SKIP: Pixel is already 0.\n");
        else {
            for (int i = 1; i <= 9; i++)
                s.append("p").append(i).append(" = ").append(p((byte) i, x, y)).append(", ");
            s = new StringBuilder(s.substring(0, s.length() - 2) + "\n").append("a = ")
                    .append(a(x, y)).append(", b = ").append(b(x, y)).append("\n");
            byte b = b(x, y);
            s.append("C1 = ").append(b >= 2 && b <= 6).append(", C2 = ").append(a(x, y) == 1);
            byte p2 = p((byte) 2, x, y), p4 = p((byte) 4, x, y), p6 = p((byte) 6, x, y), p8 = p((byte) 8, x, y);
            if (mode == 1) s.append(", C3 (Mode 1) = ").append(p2 * p4 * p6 == 0)
                    .append(", C4 (Mode 1) = ").append(p4 * p6 * p8 == 0).append("\n");
            else s.append(", C3 (Mode 2) = ").append(p2 * p4 * p8 == 0)
                    .append(", C4 (Mode 2) = ").append(p2 * p6 * p8 == 0).append("\n");
        }
        IJ.log(s.toString());
    }
}