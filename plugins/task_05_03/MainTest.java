package plugins.task_05_03;

import java.util.ArrayList;
import java.util.List;

public class MainTest {
    public static void main(String[] args) {

        int cores = Runtime.getRuntime().availableProcessors();
        cores = 3;
        // System.out.println(cores);

        List<Integer> list = new ArrayList<>();

        int width = 100;
        int pic1_start = 0;

        int jump = width / cores;


        for (int i = 0; i < cores; i++) {
            int start = i * jump;
            int end = start + jump - 1;
            if (i == cores - 1)
                end = width - 1;
            System.out.println("start: " + start);
            System.out.println("end: " + end);
        }


    }
}
