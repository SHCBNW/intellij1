import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

import java.text.MessageFormat;

/**
 * Task:
 * Implement gaussian filters of size 5x5 and 7x7, respectively, and apply
 * these filters to the noisy image. (2 BP)
 */

@SuppressWarnings({"unused", "SpellCheckingInspection", "WeakerAccess", "ConstantConditions", "SillyAssignment", "SameParameterValue", "WrongPackageStatement", "Duplicates"})
public class ex_03_02b_scheng2s implements PlugInFilter {
    private ImagePlus imp;
    private ImageProcessor ip;
    private GaussKernel g;

    private double sigma;
    private int size, width, height, roundOffHalfFilterSize;
    private long millisKernel, millisFilter;

    private boolean showGaussKernel, showCoordinates;

    public ex_03_02b_scheng2s() {
    }

    public int setup(String arg, ImagePlus img) {
        this.imp = imp;
        return DOES_ALL;
    }

    public void run(ImageProcessor ip) {
        this.ip = ip;
        showOptionsMenu();
    }

    private void showOptionsMenu() {
        GenericDialog gd = new GenericDialog("Gaussian Blur by Siu Ho Cheng");
        gd.addNumericField("Sigma", 1.0, 2, 1, "");
        gd.addNumericField("Size", 5, 0);
        gd.addCheckbox("Show Gauss Kernel", false);
        gd.addCheckbox("Show Coordinates", false);
        gd.showDialog();
        if (gd.wasCanceled()) return;
        this.millisKernel = System.currentTimeMillis();
        this.sigma = gd.getNextNumber();
        this.size = (int) gd.getNextNumber();
        this.showGaussKernel = gd.getNextBoolean();
        this.showCoordinates = gd.getNextBoolean();
        if (this.size % 2 == 0) {
            IJ.error("Wrong Filtersize", "Please select a filtersize with odd number.");
            return;
        }
        this.g = new GaussKernel(this.sigma, this.size);
        millisFilter = System.currentTimeMillis();
        coordinateHelper();
    }

    private void coordinateHelper() {
        this.width = this.ip.getWidth(); // starts count with 1
        this.height = this.ip.getHeight();
        this.roundOffHalfFilterSize = this.size / 2; // 2.6 -> 2
        ImageProcessor newPicture = this.ip.createProcessor(this.width, this.height);
        for (int picture_X = 0; picture_X < this.width; picture_X++)
            for (int picture_Y = 0; picture_Y < this.height; picture_Y++)
                newPicture.putPixel(picture_X, picture_Y, gausBlur(picture_X, picture_Y));
        millisFilter = System.currentTimeMillis() - this.millisFilter;
        // Print information in window titlebar
        new ImagePlus(MessageFormat.format("Gaussian Blur, sigma: {0}, size: {1}, time: {2} ms",
                this.sigma, this.size, millisKernel + millisFilter), newPicture).show();
    }

    private int[] gausBlur(int picture_X, int picture_Y) {
        int[] rgb;   // 0=X, 1=Y
        double sumR = 0.0, sumG = 0.0, sumB = 0.0, sumGauss = 0.0; // Reset sum values to 0.0
        int[] filterUpLeft = new int[]{ // Define upper left corner for gausBlur
                checkRange(picture_X - roundOffHalfFilterSize, this.width - 1),
                checkRange(picture_Y - roundOffHalfFilterSize, this.height - 1)};
        int[] filterLowRight = new int[]{ // Define lower right corner for gausBlur
                checkRange(picture_X + roundOffHalfFilterSize, this.width - 1),
                checkRange(picture_Y + roundOffHalfFilterSize, this.height - 1)};
        for (int filter_X = filterUpLeft[0]; filter_X <= filterLowRight[0]; filter_X++)
            for (int filter_Y = filterUpLeft[1]; filter_Y <= filterLowRight[1]; filter_Y++) {
                this.ip.getPixel(filter_X, filter_Y, rgb = new int[3]);
                // Get position in gauss kernel
                int gauss_X = filter_X - picture_X + roundOffHalfFilterSize;
                int gauss_Y = filter_Y - picture_Y + roundOffHalfFilterSize;
                if (this.showCoordinates)
                    IJ.log(MessageFormat.format("Check: ({0},{1}), Filter: ({2},{3}), Gauss: ({4},{5})",
                            picture_X, picture_Y, filter_X, filter_Y, gauss_X, gauss_Y));
                double gauss = this.g.getGauss(gauss_X, gauss_Y);
                sumGauss += gauss; // double
                sumR += rgb[0] * gauss; // double
                sumG += rgb[1] * gauss; // double
                sumB += rgb[2] * gauss; // double
            }
        return new int[]{(int) (sumR / sumGauss), (int) (sumG / sumGauss), (int) (sumB / sumGauss)};
    }

    // Helper to avoid ArrayOutOfBounds
    private int checkRange(int value, int max) {
        return (value < 0) ? 0 : (value > max) ? max : value;
    }

    // Inner class
    class GaussKernel {
        private double[][] kernalVal;

        // Constructor
        public GaussKernel(double sigma, int size) {
            init(sigma, size);
        }

        private void init(double sigma, int size) {
            this.kernalVal = new double[size][size];
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    this.kernalVal[x][y] = calc(sigma, x - size / 2, y - size / 2);
            millisKernel = System.currentTimeMillis() - millisKernel; // Stops tracking because of possible print()
            if (showGaussKernel) print();
        }

        @SuppressWarnings("UnnecessaryLocalVariable")
        private double calc(double sigma, int x, int y) {
            return (1 / (2 * Math.PI * sigma * sigma)) * Math.exp(-((x * x) + (y * y)) / (2 * sigma * sigma));
        }

        protected double getGauss(int x, int y) {
            return this.kernalVal[x][y];
        }

        @SuppressWarnings("StringConcatenationInLoop")
        protected void print() {
            String s = "";
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++)
                    s += MessageFormat.format("({0},{1}: {2}) ", x, y, getGauss(x, y));
                s += "\n";
            }
            IJ.error(MessageFormat.format("Gauss Kernel, Size: {0}x{0}, Sigma: {1}", size, sigma), s);
        }
    }
}